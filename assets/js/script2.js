/*======================

WD004-S12-DATA-STRUCTURES-7
GitLab: s12-data-structures-7

BINARY SEARCH TREE

- made up of separate objects (aka NODES)
- the first node (the top of the tree) is called the ROOT
- except for the root, nodes are referenced only by 1 node (aka a node's PARENT NODE)
- nodes are either null or have references to other TWO other nodes (aka CHILDREN)
- child nodes: left node & right node
- nodes with no children are called LEAVES

- ***** left node is always SMALLER than its parent
- ***** right node is always GREATER than its parent 
- For our application, we won't allow duplicates (meaning: the value of parent can't be equal to child)

- an ordered data structure
- allows faster insertion, deletion, and search.

- APPLICATION: search, game logic, autocomplete tasks, graphics, etc.

========================*/

function BST(){
	function Node(value) {
		this.data = value;
		this.left = null;
		this.right = null;
	}

	this.root = null;

	//ADD A NODE INTO OUR BST
	this.insert = function(value){
		//return console.log(value);
		let node = new Node(value);			//44
		//return console.log(this.root);	//null
		//this.root = node;
		const root = this.root;

		if( this.root === null ){
			this.root = node;
		} else {
			// return console.log("may laman na ang root");
		
			/*- *** left node is always SMALLER than its parent
			- *** right node is always GREATER than its parent 
			- For our application, we won't allow duplicates (meaning: the value of parent can't be equal to child) */
			//return console.log(root);		//44 -> the PARENT
			//return console.log(node);		//10 -> 

			function searchTree(root) {
				if (value < root.data) {			//data is the 'value' property   this.data = value;
					//left
					//return console.log('left');
					if(root.left === null) {
						root.left = node;			//10 (the new value being added)
					} else {
						return searchTree(root.left);
					}
				} else if (value > root.data) {
					//right
					//return console.log('right');
					if(root.right === null) {
						root.right = node;			//10 (the new value being added)
					} else {
						return searchTree(root.right);
					}
				} else {
					//duplicate
					return console.log('duplicate');
				}
			}
			searchTree(root);
		}
	}

	// FIND A NODE
	this.find = function(value){
		let currentNode = this.root;

		if(currentNode === null) {
			return console.log('The tree is empty.');
		} else if(currentNode.data == value) {
			return currentNode;
		} else {
			try {
				while(currentNode.data !== value){
					if(value < currentNode.data){
						currentNode = currentNode.left;
					} else {
						currentNode = currentNode.right;
					}
				}
				return currentNode;			//ka-match na yung hinahanap na data(node)
			} catch(e) {
				return console.log(e);
				if(e instanceof TypeError) {
					return console.log(`${value} doesn't exist.`);
				}
			}
		}
	}
}


let bst = new BST();		//instantiation
bst.insert(44);
bst.insert(5);
bst.insert(10);
bst.insert(9);
bst.insert(45);
console.log(bst.find(44));
console.log(bst);



