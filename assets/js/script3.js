function BST(){
  function Node(value) {
    this.data = value;
    this.left = null;
    this.right = null;
  }

  this.root = null;

  //ADD A NODE INTO OUR BST
  this.insert = function(value){
    //return console.log(value);
    let node = new Node(value);     //44
    //return console.log(this.root);  //null
    //this.root = node;
    const root = this.root;

    if( this.root === null ){
      this.root = node;
    } else {
      // return console.log("may laman na ang root");
    
      /*- *** left node is always SMALLER than its parent
      - *** right node is always GREATER than its parent 
      - For our application, we won't allow duplicates (meaning: the value of parent can't be equal to child) */
      //return console.log(root);   //44 -> the PARENT
      //return console.log(node);   //10 -> 

      function searchTree(root) {
        if (value < root.data) {      //data is the 'value' property   this.data = value;
          //left
          //return console.log('left');
          if(root.left === null) {
            root.left = node;     //10 (the new value being added)
          } else {
            return searchTree(root.left);
          }
        } else if (value > root.data) {
          //right
          //return console.log('right');
          if(root.right === null) {
            root.right = node;      //10 (the new value being added)
          } else {
            return searchTree(root.right);
          }
        } else {
          //duplicate
          return console.log('duplicate');
        }
      }
      searchTree(root);
    }
  }

  // FIND A NODE
  this.find = function(value){
    let currentNode = this.root;

    if(currentNode === null) {
      return console.log('The tree is empty.');
    } else if(currentNode.data = value) {
      return currentNode;
    } else {
      try {
        while(currentNode.data !== value){
          if(value < currentNode.data){
            currentNode = currentNode.left;
          } else {
            currentNode = currentNode.right;
          }
        }
        return currentNode;     //ka-match na yung hinahanap na data(node)
      } catch(e) {
        return console.log(e);
        if(e instanceof TypeError) {
          return console.log(`${value} doesn't exist.`);
        }
      }
    }
  }


  /*
    ACTIVITY 1:
    Create a findMin method that finds the minimum value in your tree. 

    Note: The node with the minimum value is at the LEFT side of your root. You need to traverse the children in it. 
    Once you've run out of left children to traverse, you've found the minimum value of your tree.
  */
  // FIND MIN
  this.findMin = function(){
    let currentNode = this.root;

    //console.log("currentNode.left")
    //console.log(currentNode.left);

    if(currentNode === null) {
      return console.log('The tree is empty.');
    }
    // } else if(currentNode.data = value) {
    //   return currentNode;
    // } else {
    //   try {
      //console.log(currentNode.left);
        while(currentNode.left !== null){
          //if(currentNode.data > currentNode.left.data){
            //console.log(currentNode.data);
            //console.log(currentNode.data.left);
            currentNode = currentNode.left;
          //}
            //console.log("currentNode.left while else")
            //return currentNode.data;
        }
        return currentNode.data;
        //return currentNode;
      // } catch(e) {
      //   return console.log(e);
      //   if(e instanceof TypeError) {
      //     return console.log(`${value} doesn't exist.`);
      //   }
      // }
    //}
  }


  /*
    ACTIVITY 2:
    Create a findMax method that finds the maximum value in your tree.

    Note: The node with the maximum value is at the RIGHT side of your root. 
  */
  // FIND MAX
  this.findMax = function(){
    let currentNode = this.root;

    //console.log("currentNode.left")
    //console.log(currentNode.left);

    if(currentNode === null) {
      return console.log('The tree is empty.');
    }
    // } else if(currentNode.data = value) {
    //   return currentNode;
    // } else {
    //   try {
      //console.log(currentNode.right);
        while(currentNode.right !== null){
          //if(currentNode.data > currentNode.left.data){
            //console.log(currentNode.data);
            //console.log(currentNode.data.left);
            currentNode = currentNode.right;
          //}
            //console.log("currentNode.left while else")
            //return currentNode.data;
        }
        return currentNode.data;
        //return currentNode;
      // } catch(e) {
      //   return console.log(e);
      //   if(e instanceof TypeError) {
      //     return console.log(`${value} doesn't exist.`);
      //   }
      // }
    //}
  }


/*
  ACTIVITY 3:
  Create an isPresent method that returns TRUE or FALSE depending on whether the passed in data is in the tree or not.
*/
  // FIND PRESENT
  this.iPresent = function(value){
    let currentNode = this.root;

    if(currentNode === null) {
      return console.log('The tree is empty.');
    } else if(currentNode.data == value) {
      return true;
      //return currentNode;
    } else {
      try {
        while(currentNode.data !== value){
          if(value < currentNode.data){
            currentNode = currentNode.left;
          } else {
            currentNode = currentNode.right;
          }
        }
        return true;
        //return currentNode;     //ka-match na yung hinahanap na data(node)
      } catch(e) {
        //return console.log(e);
        return false;
        // if(e instanceof TypeError) {
        //   return console.log(`${value} doesn't exist.`);
        //}
      }
    }
  }
}




let bst = new BST();    //instantiation
bst.insert(44);
bst.insert(10);
bst.insert(45);
//console.log(bst.find(44));
bst.insert(13);
bst.insert(5);
bst.insert(11);
bst.insert(6);
bst.insert(49);
console.log("Min is " + bst.findMin());
console.log("Max is " + bst.findMax());
console.log("iPresent? " + bst.iPresent(15));
//console.log(bst.findMin());
// console.log(bst.find());
console.log(bst);



  /*
    ACTIVITY 1:
    Create a findMin method that finds the minimum value in your tree. 

    Note: The node with the minimum value is at the LEFT side of your root. You need to traverse the children in it. 
    Once you've run out of left children to traverse, you've found the minimum value of your tree.
  */

  
  /*
    ACTIVITY 2:
    Create a findMax method that finds the maximum value in your tree.

    Note: The node with the maximum value is at the RIGHT side of your root. 
  */
  
    /*
    ACTIVITY 3:
    Create an isPresent method that returns TRUE or FALSE depending on whether the passed in data is in the tree or not.
  */




